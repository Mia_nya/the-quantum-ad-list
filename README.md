### *We Make StevenBlack And Lightswitch05 Jealous™*

# ✌ Howdy! - The Quantum Ad-List
*This is the greatest Ad-list of all time*

<a href="https://fosstodon.org/@The_Quantum_AdList"><img src="https://files.mastodon.online/media_attachments/files/105/683/348/348/762/593/original/c54bcb52bcd6ac60.png" alt="Mastodon" style="border:0"></a> <a href="https://gitlab.com/The_Quantum_Alpha/the-quantum-ad-list/"><img src="https://files.mastodon.online/media_attachments/files/105/632/005/399/506/391/original/1d998e250bc88aca.png" alt="Join the fight for privacy, join the war against ads!" style="border:0"></a>  




#### 📑 Index
1. [Tip us!](https://gitlab.com/The_Quantum_Alpha/the-quantum-ad-list#-tip-us)
2. [Important notice](https://gitlab.com/The_Quantum_Alpha/the-quantum-ad-list#-important-notice)
3. [About The Quantum Ad-List](https://gitlab.com/The_Quantum_Alpha/the-quantum-ad-list#%E2%84%B9-about-the-quantum-ad-list)
    - [Maintainers](https://gitlab.com/The_Quantum_Alpha/the-quantum-ad-list#-maintainers)
    - [What the heck is that?](https://gitlab.com/The_Quantum_Alpha/the-quantum-ad-list#-what-the-heck-is-that)
    - [Why?](https://gitlab.com/The_Quantum_Alpha/the-quantum-ad-list#-why)
    - [Is it safe?](https://gitlab.com/The_Quantum_Alpha/the-quantum-ad-list#-is-it-safe)
4. [Installation](https://gitlab.com/The_Quantum_Alpha/the-quantum-ad-list#-installation)
    - [For your hosts file](https://gitlab.com/The_Quantum_Alpha/the-quantum-ad-list#-for-your-hosts-file)
        - [Linux](https://gitlab.com/The_Quantum_Alpha/the-quantum-ad-list#-linux)
    - [For Ad Blockers](https://gitlab.com/The_Quantum_Alpha/the-quantum-ad-list#-for-ad-blockers)
        - [Vivaldi browser](https://gitlab.com/The_Quantum_Alpha/the-quantum-ad-list#-vivaldi-browser)
5. [Issues](https://gitlab.com/The_Quantum_Alpha/the-quantum-ad-list#-issues)
    - [Please report issues!](https://gitlab.com/The_Quantum_Alpha/the-quantum-ad-list#-please-report-issues)
    - [Known Issues](https://gitlab.com/The_Quantum_Alpha/the-quantum-ad-list#-known-issues)
6. [Stay in touch!](https://gitlab.com/The_Quantum_Alpha/the-quantum-ad-list#-stay-in-touch)
    - [Join TechnoChat!](https://gitlab.com/The_Quantum_Alpha/the-quantum-ad-list#-join-technochat)

# 💲 Tip us!
**Buy us a coffee... Or most likely a tea, and strawberries - lots of strawberries!**

Your support is greatly appreciated! Keeps us motivated!

[![Buy me a tea and strawberries!][buymeacoffee-shield]][buymeacoffee]

[buymeacoffee-shield]: https://www.buymeacoffee.com/assets/img/guidelines/download-assets-sm-3.svg
[buymeacoffee]: https://www.buymeacoffee.com/thequantumalpha

*$5 once, or $2/month*

## ⚠ Important notice
### ⚖ False copyright claims are forbidden by the law. It is unethical and irresponsible. 
If you send a DMCA takedown notice that is both false and meant in bad faith (such as to harass, or isn't a real claim), you have committed perjury. 

> A copyfraud is a false copyright claim by an individual or institution with respect to content that is in the public domain. Such claims are wrongful, at least under U.S. and Australian copyright law, because material that is not copyrighted is free for all to use, modify and reproduce. [Source](https://en.wikipedia.org/wiki/Copyfraud)

**Dear users,**

**Some are recently trying to silence us, feeling "endangered" by our list.**

**We repeat, this is AI generated, we won't touch your tiny list, feel assured.**

**If your "test domain" is visible on the internet, like in a doc or simply available, it will scan it.**

**If it deems potentially unwanted, or if it understand from within the context of the page (remember... AI!) it will block it !**

**If the AI finds a list containing domains (or if it simply sees other domains), it will check them all out! If your list was in the way, it is possible this might have been sort of "learned".**

**This is the work of an Artificial Intelligence.** ***There is therefore no source to claim.*** **It is fair use.**

**Ads are ads. You cannot claim copyright on an ad domain! There's already one -- by the original holder.**

***There are legally no reasons at all to strike us.***

**Thank you for understanding!**

## ℹ About The Quantum Ad-List
#### 🛠 Maintainers
This project's maintainers are The_Quantum_Alpha and Pinkisjustnumbers.
Founded by The_Quantum_Alpha

***No, we will NOT release the source code of the AI. It is proprietary.***

#### 🤨 What the heck is that?
Made an AI to track and analyse every websites, a bit like a web crawler, to find and identify ads.
It is a list containing over 1300000 domains used by **ads**, **trackers**, **miners**, **malwares**, and much more! 

It is specifically designed for **hosts** file, but can also be used with ad-blockers with the ad-blocker "optimized" variant.

We were testing an AI that could show some basic emotions about internet content, and turns out it was very precise at getting “annoyed” by ads and “unsolicited” third party connections…

From that, I forked our own project and tweaked it in a specific way to basically only focus on ads, trackers, etc. and act like a web crawler, turns out to be very effective!

Since I am more old school, the HOSTS file is still my prefered way to deal with name resolutions, thus allocating only a small amount of time for this project as it is not a priority, I decided to go for one method that could be used for all.

The lists can be used on almost anything, but I only show one way due to reasons mentioned above.

**To learn more, please join our chat with the provided link below!**

#### ❓ Why?
**To block ads!!**

Well, this list has some domains that others don't have!

#### 🛡 Is it safe?
It is completely safe. 

It does not send any of your datas, indeed it protects them, as trackers and malwares cannot enter your device.

## 📥 Installation
### 🧱 For your **hosts** file:
***This is the recommended way to use The Quantum Ad-List.***
#### 🐧 Linux:
1. You may **download** the [installer here](https://gitlab.com/The_Quantum_Alpha/the-quantum-ad-list/-/raw/master/quantum_adlist.sh?inline=false)
2. Open a terminal, then navigate to the directory where the installer named `quantum_adlist.sh` is located. (`cd ~/Downloads` if that’s where you downloaded it).
3. Now run the following command to make the installer executable: `chmod u+x quantum_adlist.sh`
4. Execute `sudo ./quantum_adlist.sh`
5. Voilà!

### 🚫 For Ad-blockers:
*Please keep in mind it is not meant to be used that way. Use with caution.*
#### 🌎 Vivaldi browser:
1. Open the browser's settings.
2. Head over to the "Privacy" section, then scroll down to see "Blocking trackers and ads" or such.
3. Hit the <kbd>Manage sources</kbd> button, then scroll down to see "Sources of Ad Blocking" or such.
4. Hit the <kbd>+</kbd> button, a small popup should appear in top middle of Vivaldi. 
5. **Paste** the link of the lists you want to use.
7. Restart Vivaldi, and voilà!

## 👾 Issues
#### 📬 Please report issues!
If you encounter any issues, such as wrongfully blocked domains, please report them to us!

Since it is the work of Artificial Intelligence, this list is certainly not perfect! Your help is greately appreciated!

#### 🏷 Known issues

| Issue | Status | Priority | Possible temporary workaround |
| ----- | ----- | ----- | ----- |
| YouTube loading problem | Under investigation | Low | May avoid using The Quantum Extreme YouTube Ad-List |


## 👥 Stay in touch!
#### 💬 Join TechnoChat!
[Join us on MeWe today!](https://mewe.com/join/TechnoChat)

<a href="https://mewe.com/join/TechnoChat"><img src="https://img.mewe.com/api/v2/group/5eab27fa87cee22e5cad5eab/public-image/60112d1ab858e357235286a6/1600x1600/img" alt="Join TechnoChat" style="border:0"></a>

<a rel="me" href="https://fosstodon.org/@The_Quantum_AdList"></a>
